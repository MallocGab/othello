#ifndef JEU_H
#define JEU_H
#define NBSCORE 3

#include <vector>
#include <iostream>
#include <fstream>
#include <utility>
#include <sstream>
#include <string>

#include "Pion.h"
#include "Joueur.h"
#include "IA.h"
#include "CaseJouable.h"
#include "Recherche.h"
#include "Arbre.h"


class Jeu
{
    public:
        Jeu();
        ~Jeu();
        //Getters and setters
        std::vector<std::vector<Pion*>> getPlateau() { return m_plateau; }
        void setPlateau(std::vector<std::vector<Pion*>> val) { m_plateau = val; }
        void setCoupsJouables(std::vector<CaseJouable> val) { m_coupsjouables = val;}

        bool getisOver() { return m_isOver; }
        void setisOver(bool val) { m_isOver = val; }

        std::vector<CaseJouable> getCoupsJouable() {return m_coupsjouables; }

        //Fonctions
        void initialisationPlateau();
        void affichagePlateau();
        void affichemenu();
        void save();
        void load();
        int menu();
        void choixNiveau();
        void jeu(int &niveau);
        void choixCouleurJcJ(Joueur &j1, Joueur &j2);
        int choixCouleur(Joueur &joueur);
        void tourDeJeuJoueur(Joueur &joueur);
        void canPlay(int couleur);

        void tourDeJeuIA(int couleur);

        void Comptescore ();
        void afficheCaseJouable ();
        void caseJouableTab ();

        void endGame();


    private:
        std::vector<std::vector<Pion*>> m_plateau;

        //� reinitialiser a chaque tour
        std::vector<CaseJouable> m_coupsjouables;

        std::vector<int> m_score;


        bool m_isOver = false;
        int m_niveau;
        int m_tour = 1;
        int m_hasAbandonned = 0;
        int m_hasWon = 0;

        int m_firstPass = 0;
        int m_secondPass = 0;
        int m_nbpassage = 0;
        bool m_egalite = false;

        bool m_load = false;
        bool m_affichearbre = false;

};

#endif // JEU_H
