 #ifndef JOUEUR_H
#define JOUEUR_H


class Joueur
{
    public:
        Joueur();
        ~Joueur();

        //getters et setters
        unsigned int getScore() { return m_score; }
        void setScore(unsigned int val) { m_score = val; }
        int getCouleurJoueur() { return m_couleurJoueur; }
        void setCouleurJoueur(int val) { m_couleurJoueur = val; }

        //fonctions


    protected:

    private:
        unsigned int m_score;
        int m_couleurJoueur;

};

#endif // JOUEUR_H
