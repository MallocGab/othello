#include "Jeu.h"
#include <windows.h>

Jeu::Jeu()
{
    //ctor
    m_niveau = 0;
}

Jeu::~Jeu()
{
    for(const auto & elem1 : m_plateau)
    {
        for(const auto & elem2 : elem1)
        {
            delete elem2;
        }
    }
}

void Jeu::initialisationPlateau()
{
    //code venant du projet Siam de Fran�ois Ren et de Gabriel Padis du premier semestre d'ing2

    m_plateau.resize(TAILLE, std::vector<Pion*>(1 << (TAILLE-1),0));

    for (int i = 0; i < TAILLE; i ++)
    {
        for (int j = 0; j < TAILLE; j ++)
        {
            m_plateau[i][j] = nullptr;
        }
    }


//    std::cout << m_load << std::endl;
    if (m_load)
    {
        this->load();
    }
    else
    {
//        std::cout << "de base 2" << std::endl;
        //allocation des 4 pi�ces de d�part
        m_plateau[3][3] = new Pion(COLOR_WHITE); //premier [] est la ligne, le deuxi�me [] est la colonne
        m_plateau[4][4] = new Pion(COLOR_WHITE);
        m_plateau[3][4] = new Pion(COLOR_BLACK);
        m_plateau[4][3] = new Pion(COLOR_BLACK);

    }




    //initialisation du vector de score
    for(int i = 0; i < NBSCORE; i++)
    {
        m_score.push_back(0);
    }

    m_isOver = false;
    m_tour = 1;
    m_hasAbandonned = 0;
    m_hasWon = 0;

    m_firstPass = 0;
    m_secondPass = 0;
    m_nbpassage = 0;
    m_egalite = false;
}


void Jeu::affichagePlateau()
{
    /* A FAIRE : AFFICHER LES CASES JOUABLES PAR UN POINT
    */
    Console* pConsole = NULL;
    pConsole = Console::getInstance();

    /* Code issu du projet du premier semestre de Francois Ren et Gabriel Padis et r�adapt� � la situation
    */


    int pos_x = 0;
    int pos_y = 0;

    // posx posy sont les coords o� commencer a afficher le plateau
    // x et y sont coords relatives au plateau pour placer les pieces
    int x,y;


    ///affichage des bords du plateau
    pConsole->gotoLigCol(pos_x, pos_y);

    /*
        PETIT TABLEAU
    */
    /*
    std::ifstream ifs("Terrain.txt");
    if (ifs) // test si le fichier est bien ouvert
    {
        std::string line;
        while (std::getline(ifs, line)) // lecture ligne par ligne
        {
            std::cout << line << std::endl;
        }
    //        std::cout << "Reading " << path << " => OK" << std::endl;
        ifs.close(); // fermeture du flux
    }
    else // en cas d'erreur...
    {
        std::cout << "Cannot read " << std::endl;
    }

    ///affichage du plateau de jeu

    //POSITION du curseur sur le plateau du milieu

    y=pos_y+2; // coords de la premiere case
    x=pos_x+3; //
    pConsole->gotoLigCol(y, x);

    //parcours du vecteur de pieces
    for (int i = 0; i < 8; i ++)
    {
        for (int j = 0; j < 8; j ++)
        {
            pConsole->gotoLigCol(y,x);

            //pConsole->setColor(COLOR_WHITE);
            pConsole->setColorMod(15,2);
            //pConsole->colorierCase(y,x,15,2);

            if(m_plateau[i][j]!=nullptr)
            {
                if(m_plateau[i][j]->getCouleur() == 1)
                {
                    //couleur blanche

                    //pConsole->setColor(COLOR_WHITE);
                    pConsole->  (15,2);

                    std::cout << m_plateau[i][j]->getSymbole() << " ";
                }
                else if(m_plateau[i][j]->getCouleur() == 2)
                {
                    //couleur noire

                    //pConsole->setColor(COLOR_WHITE);
                    pConsole->setColorMod(0,2);

                    std::cout << m_plateau[i][j]->getSymbole() << " ";
                }
            }
            else std::cout << "  ";

            x+=3;

        }
        //remet le x au debut
        x=pos_x+3;
        y+=1;
        pConsole->gotoLigCol(y, x);
    }
    */

    /*
        GRAND TABLEAU
    */
    pConsole->setColor(COLOR_DEFAULT);

    std::ifstream ifs("Terrain_Old.txt");
    if (ifs) // test si le fichier est bien ouvert
    {
        std::string line;
        while (std::getline(ifs, line)) // lecture ligne par ligne
        {
            std::cout << line << std::endl;
        }
        ifs.close(); // fermeture du flux
    }
    else // en cas d'erreur...
    {
        std::cout << "Cannot read " << std::endl;
    }

    ///affichage du plateau de jeu

    //POSITION du curseur sur le plateau du milieu

    y=pos_y+2; // coords de la premiere case
    x=pos_x+3; //
    pConsole->gotoLigCol(y, x);

    //parcours du vecteur de pieces
    for (int i = 0; i < TAILLE; i ++)
    {
        int tempY = y++;
        for (int j = 0; j < TAILLE; j ++)
        {
            pConsole->gotoLigCol(y,x);

            pConsole->setColorMod(15,2);//blanc sur vert

            if(m_plateau[i][j]!=nullptr)
            {
                if(m_plateau[i][j]->getCouleur() == COLOR_WHITE)
                {
                    //couleur blanche

                    pConsole->setColorMod(15,2); //blanc sur vert

                    std::cout << "     \n";
                    pConsole->gotoLigCol(tempY, x);
                    std::cout << "  " << m_plateau[i][j]->getSymbole() << "  \n";

                    pConsole->gotoLigCol(tempY+1, x);
                    std::cout << "     \n";

                    //solution terrain completement vert: remplacer les 2 lignes imm�diatement au dessus
                    /*pConsole->gotoLigCol(tempY+2, x);_
                    std::cout << "     \n";*/
                }
                else if(m_plateau[i][j]->getCouleur() == COLOR_BLACK)
                {
                    //couleur noire

                    pConsole->setColorMod(0,2);//noir sur vert

                    std::cout << "     \n";
                    pConsole->gotoLigCol(tempY, x);
                    std::cout << "  " << m_plateau[i][j]->getSymbole() << "  \n";
                    pConsole->gotoLigCol(tempY+1, x);
                    std::cout << "     \n";
                }
            }
            else
            {
                std::cout << "     \n";
                pConsole->gotoLigCol(tempY, x);
                std::cout << "     \n";
                pConsole->gotoLigCol(tempY+1, x);
                std::cout << "     \n";
            }

            x+=6;

        }
        //remet le x au debut
        x=pos_x+3;
        y+=2;
        pConsole->gotoLigCol(y, x);
    }
    pConsole->setColor(COLOR_DEFAULT);
    caseJouableTab();

    //A NE PAS DECOMMENTER
    //delete pConsole;

}

void Jeu::save()
{
    std::ofstream file ("save.txt");

/*    file << couleurJoueur << std::endl;
    if (couleurJoueur == COLOR_BLACK) file << COLOR_WHITE << std::endl;
    else if (couleurJoueur == COLOR_WHITE) file << COLOR_BLACK << std::endl;
        */

    for(int i = 0; i < TAILLE; i++)
    {
        for(int j = 0; j < TAILLE; j++)
        {
            if(m_plateau[i][j]!=nullptr)
            {
                file << m_plateau[i][j]->getCouleur() << " ";
            }
            else
            {
                file << 0 << " ";
            }
        }
        file << std::endl;
    }

    file.close();
}

void Jeu::load()
{
    int temp;
    std::ifstream file("save.txt", std::ios::in);
    for(int i = 0; i < TAILLE; i++)
    {
        for(int j = 0; j < TAILLE; j++)
        {
            file >> temp;
            if(temp == 0) m_plateau[i][j] = nullptr;
            else
            {
                m_plateau[i][j] = new Pion(temp);
            }
        }
    }
    file.close();
}

int Jeu::menu()
{

    int choix;
    bool choixFait = false;

    while(!choixFait)
    {
         affichemenu();
         initialisationPlateau();

            std::cin >> choix;

            switch(choix)
            {
            case 1 :
                this->choixNiveau();
                choixFait = true;
                break;
            case 2:
                m_load = true;
                this->choixNiveau();
                choixFait = true;
                break;
            case 3 :
                return 1;
                choixFait = true;
                break;
            default :
               // choixFait = false;
                std::cout << "Saisie invalide : veuillez resaisir votre choix : ";
                break;
            }

    }
    return 0;
}

void Jeu::choixNiveau()
{
    bool niveauFait = false;

    std::cout << "Quel niveau de difficulte ?" <<std::endl;
    std::cout << "Niveau 1 (IA aleatoire)\nNiveau 2 (IA MinMax)\nNiveau 3 (IA NegaMax)\nNiveau 4 (Joueur VS Joueur)" << std::endl;
    std::cin >> m_niveau;
    while(!niveauFait)
    {
        if (m_niveau >= 1 && m_niveau <= 4)
        {
            initialisationPlateau();
            jeu(m_niveau);
            niveauFait = true;
        }
        else
        {
            std::cout <<"Saisie invalide, veuillez recommencer votre saisie : ";
            std::cin >> m_niveau;
        }
    }
}


int Jeu::choixCouleur(Joueur &joueur)
{
    bool choixValide = false;
    int couleur;

    std::cout << "Veuillez choisir votre couleur : \n1)Blanc\n2)Noir\nAttention ce sont les noirs qui commencent\n" << std::endl;
    std::cin >> couleur;
//    std::cout<< "coul"<<couleur;
    while(!choixValide)
    {
        if(couleur == COLOR_WHITE || couleur == COLOR_BLACK)
            choixValide = true;
        else
        {
            std::cout << "Choix invalide, veuillez entrer de nouveau votre choix : ";
            std::cin >> couleur;
        }
    }

    if(couleur == COLOR_WHITE)
    {
//        std::cout<< "coul"<<couleur;
        joueur.setCouleurJoueur(COLOR_WHITE);
        return COLOR_BLACK;

    }
    else
    {
//        std::cout<< "coul"<<couleur;
        joueur.setCouleurJoueur(COLOR_BLACK);
        return COLOR_WHITE;
    }
//    std::cout << joueur.getCouleurJoueur() << std::endl;
//    std::cout << ia.getCouleurIA() << std::endl;
}
/// A peu pres un copier coller de la fonction choixCouleur mais ici pour 2 joeuur
void Jeu::choixCouleurJcJ(Joueur &j1, Joueur &j2)
{
    bool choixValide = false;
    int couleur;

    std::cout << "Couleur du JOUEUR 1 : \n1)Blanc\n2)Noir\nAttention ce sont les noirs qui commencent\n" << std::endl;
    std::cin >> couleur;
    while(!choixValide)
    {
        if(couleur == COLOR_WHITE || couleur == COLOR_BLACK)
            choixValide = true;
        else
        {
            std::cout << "Choix invalide, veuillez entrer de nouveau votre choix : ";
            std::cin >> couleur;
        }
    }

    if(couleur == COLOR_WHITE)
    {
        j1.setCouleurJoueur(COLOR_WHITE);
        j2.setCouleurJoueur(COLOR_BLACK);

    }
    else
    {
        j1.setCouleurJoueur(COLOR_BLACK);
        j2.setCouleurJoueur(COLOR_WHITE);
    }
}

void Jeu::jeu(int &niveau)
{
    //system("cls");
    Joueur joueur;
    Joueur joueur2;
    IA ia;
    m_tour = 1;
    Console* Pligcol = NULL;
    // Joueur contre Joueur
    if(niveau == 4)
    {
        choixCouleurJcJ(joueur,joueur2);

        while(!m_isOver)
        {

            system("cls");

            if(m_tour == 1)
            {
                Pligcol->gotoLigCol(5,70);
                std::cout<<"TOUR DU NOIR";
                if(joueur.getCouleurJoueur() == COLOR_BLACK)    tourDeJeuJoueur(joueur);
                else    tourDeJeuJoueur(joueur2);
            }
            if(m_tour == -1)
            {
                Pligcol->gotoLigCol(5,70);
                std::cout<<"TOUR DU BLANC";
                if(joueur.getCouleurJoueur() == COLOR_WHITE)    tourDeJeuJoueur(joueur);
                else    tourDeJeuJoueur(joueur2);
            }
            m_tour=m_tour*(-1);
        }
    }
    else //Joueur contre IA
    {

        ia.setCouleurIA(this->choixCouleur(joueur));

        system("cls"); //nettoyer l'�cran de l'affichage du d�but

        while(!m_isOver)
        {

            //        std::cout<<"col"<<joueur.getCouleurJoueur();
            //au noir de jouer
            //        std::cout << joueur.getCouleurJoueur();

            if(joueur.getCouleurJoueur() == COLOR_BLACK) //si le joueur a les noirs alors il joue
            {
                this->tourDeJeuJoueur(joueur);
            }
            else //sinon c'est que c'est l'IA qui est noir et qui doit jouer
            {
                this->tourDeJeuIA(ia.getCouleurIA());
            }

            //au blanc de jouer
            if(joueur.getCouleurJoueur() == COLOR_WHITE) //si le joueur a les blancs
            {
                this->tourDeJeuJoueur(joueur);
            }
            else
            {
                this->tourDeJeuIA(ia.getCouleurIA());
            }
            //std::cout<<"dtrdtrdtdrtdtrd";
            //        getch();

            m_tour++;
        }
    }

    endGame();

    delete Pligcol;
}

void Jeu::tourDeJeuJoueur(Joueur & p)
{

    //TOUR JOUEUR

    Recherche recherche(p.getCouleurJoueur(), m_plateau); //calcul automatique des coups possibles gr�ce au constructeur

    Recherche recherche2(p.getCouleurJoueur(), recherche.getCoupsjouables(), m_plateau);

    Console* Pligcol = NULL;
    int ligaffichage = 10;

    //A chaque debut de tour je reinitialise mon vecteur de coups jouable a 0 cases
    std::vector<CaseJouable> nouveautab;
    setCoupsJouables(nouveautab);

    Pligcol->gotoLigCol(ligaffichage,70);


    //on appel la classe Recherche afin de calculer les coups jouables
    //recherche.calculdescoupjouables(p.getCouleurJoueur());
    m_coupsjouables = recherche.getCoupsjouables();

    canPlay(p.getCouleurJoueur()); //passage de tour ?

    //  for(auto elem : m_coupsjouables)
    // {
    //   std::cout<<"COUPS JOUABLE : lig "<<elem.getLig()+1<<" ; col "<<elem.getCol()+1;
    //    ligaffichage++;
    //    Pligcol->gotoLigCol(ligaffichage,70);
    // }
//    Pligcol->gotoLigCol(20,70);
//    std::cout<<"APRES CALCUL POSSIBLE JOUEUR";

    affichagePlateau();

    Comptescore();
    //afficheCaseJouable(p);


//    std::cout << "C'est au tour du joueur de jouer :" << std::endl;


    //selection

    //ajout pion

    //application de l'ajout

    //affichage

    bool pionok = false;
    int i=11;
    int j=23;
    int imatrice = 3;
    int jmatrice = 3;

    int touche;

    if(m_secondPass == 0) //s'il n'y a pas encore eu 2 fois impossibilit� de jouer d'affile
    {

        if (!(m_firstPass == p.getCouleurJoueur())) //si le joueur ne doit pas passer son tour
        {
            if(m_hasWon == 0) //si n'a pas encore gagner on rentre dans la boucle
            {
                do
                {
                    Pligcol -> gotoLigCol(i,j);
                    if(kbhit())
                    {
                        touche=getch(); // enregistre la touche saisie au clavier
                        switch (touche)
                        {
                        ///d�placer curseur vers le bas
                        case 's':
                            if (i>=0 && i<23)
                            {
                                pionok = false;
                                i= i+3;
                                imatrice+=1;
                                Pligcol -> gotoLigCol(i,j);

                            }

                            break;
                        ///deplacer le curseur vers la gauche
                        case 'q' :
                            if (j>6 && j<=47)
                            {
                                pionok = false;
                                j=j-6;
                                jmatrice-=1;
                                Pligcol -> gotoLigCol(i,j);

                            }


                            break;

                        /// d�placer le curseur vers la droite
                        case 'd':
                            if (j>=0 && j<47)
                            {
                                pionok = false;
                                j= j+6;
                                jmatrice+=1;
                                Pligcol -> gotoLigCol(i,j);

                            }


                            break;

                        /// d�placer le curseur vers le haut
                        case 'z':
                            if (i>3 && i<=23)

                            {
                                pionok = false;
                                i = i-3;
                                imatrice-=1;
                                Pligcol -> gotoLigCol(i,j);
                            }

                            break;

                        /// ajouter le pion
                        case 'o' :
//                std::cout<<"imatrice : "<<imatrice<<"  jmatrice : "<<jmatrice<<std::endl;
//                std::cout<<"AJOUT PION OK ?"<<
                            if (recherche2.ajouterPion(imatrice,jmatrice,p.getCouleurJoueur(),m_plateau))
                            {//m_plateau = recherche2.getPlateau();
                                pionok = true;
            //                std::cout<<"CACA";
                            }
//                system("pause");
                            break;
                        case 'a' : //abandon
                            m_hasAbandonned = p.getCouleurJoueur();
                            m_isOver = true;
                            pionok = true;
                            break;

                        case 'e' : //enregistrer
                            save();
                            m_isOver = true;
                            pionok = true;
                            break;

                        case 'n':
                            if(m_affichearbre == true) m_affichearbre = false;
                            else m_affichearbre = true;
                            Pligcol->gotoLigCol(15,70);
                            std::cout<<"Afficher l'arbre ? "<<m_affichearbre;
                            break;
                        }
//           Pligcol -> setColorMod(12,0);
//           affichagePlateau();
                    }
                }
                while(pionok == false);
            }
        }
    }

    delete Pligcol;
}

void Jeu::tourDeJeuIA(int couleur)

{
    //TOUR IA

    Recherche recherche(couleur, m_plateau);

    Console* Pligcol = NULL;

    IA ia(couleur);

    //A chaque debut de tour je reinitialise mon vecteur de coups jouable a 0 cases
    std::vector<CaseJouable> nouveautab;


    setCoupsJouables(nouveautab);

//    Pligcol->gotoLigCol(14,70);
//    std::cout<<"AVANT CALCUL POSSIBLE IA";

    //recherche.calculdescoupjouables(ia.getCouleurIA());
    m_coupsjouables = recherche.getCoupsjouables();

//    Pligcol->gotoLigCol(14,70);
//    std::cout<<"APRES CALCUL POSSIBLE IA";

    affichagePlateau();

    ///afficheCaseJouable(p);

    canPlay(couleur);

    Comptescore();
    //Pligcol->gotoLigCol(70,10);
    // std::cout << "C'est au tour de l'IA de jouer : " << std::endl;
    ///  int ligaffiche =14;
    /// for(auto elem : m_coupsjouables)
    ///  {
    ///     std::cout<<"COUPS JOUABLE : lig "<<elem.getLig()+1<<" ; col "<<elem.getCol()+1<<std::endl;
    ///    ligaffiche++;
    ///    Pligcol->gotoLigCol(ligaffiche,70);
    ///}

//    system("pause");

    Pligcol->gotoLigCol(1,75);

    CaseJouable caseAJouer;

    ia.setMax(0);

    if(m_secondPass == 0) //s'il n'y a pas encore eu 2 fois impossibilit� de jouer d'affile
    {

        if (!(m_firstPass == ia.getCouleurIA()))  //si l'ia ne doit pas passer son tour
        {
            switch(m_niveau)
            {
            case 1 :
                {
//                    Arbre a(new Noeud(m_plateau,0,1));
//                    a.creerArbre(5);
//                    a.afficherArbre(150,0);

//                    for(auto elem:a.getNoeuds())
//                    {
//                        std::cout<<"p:"<<elem->getProfondeur()<<"-"<<elem->getHauteur()<<" ## ";
//                    }

                    getch();
                    caseAJouer = ia.aleatoire(m_coupsjouables);
                    break;
                }
            case 2 :
                caseAJouer = ia.minMax2(m_plateau,5,m_affichearbre); //selection min max
                break;
            case 3 :
                caseAJouer = ia.appelNegaMAx(m_plateau,5,m_affichearbre);//selection amelioration
                break;
            }

            //joue cette pi�ce
//            std::cout<<"caseAJouer("<<caseAJouer.getLig()+1<<";"<<caseAJouer.getCol()+1<<") couleurIA:"<<ia.getCouleurIA()<<std::endl;
//            system("pause");
            recherche.ajouterPion(caseAJouer.getLig(),caseAJouer.getCol(),ia.getCouleurIA(),m_plateau);

                //m_plateau = recherche.getPlateau();
//                std::cout<<"ajout successful"<<std::endl;
//                system("pause");


            //affichage
//            std::cout<<"AFFICHAGE PLAT"<<std::endl;
//            getch();
            affichagePlateau();
//            std::cout<<"AFFICHAGE PLAT OK"<<std::endl;
//            system("pause");
            Pligcol->gotoLigCol(10,70);
            std::cout<<"Case jouee par IA : ("<<caseAJouer.getLig()+1<<";"<<caseAJouer.getCol()+1<<")";

        }
//    Pligcol->gotoLigCol(5,70);
//    std::cout << "ia a finit de jouer" << std::endl;

    }

    delete Pligcol;
}

void Jeu::Comptescore ()
{

    m_score [0]= 0;
    m_score [1]= 0;
    // Pion p;

    /*for (int i; i<8 ; i++)
    {
       for (int j; j<8;j++)
       {
           if (m_plateau[i][j]->getCouleur() == COLOR_BLACK)
           {
               m_score [0]= m_score [0]+1;

           }
           if (m_plateau[i][j]->getCouleur() == COLOR_WHITE)
           {
               m_score [1]=m_score [1]+1;
           }
       }
    }
    std::cout << "noir : "<<m_score[0] << " et blanc : " << m_score[1] <<std::endl;*/


    Console* Pligcol = NULL;

    for(int i = 0; i < NBSCORE; i++)
    {
        m_score[i] = 0;
    }



    // Pion p;

    for (int i = 0; i < TAILLE; i ++)
    {
        for (int j = 0; j < TAILLE; j ++)
        {
            if(m_plateau[i][j]!=nullptr)
            {
                m_score[m_plateau[i][j]->getCouleur()] ++;

                /* font exactement la m�me chose
                if (m_plateau[i][j]->getCouleur() == COLOR_BLACK)
                {
                    m_score [COLOR_BLACK]++;
                }
                else if (m_plateau[i][j]->getCouleur() == COLOR_WHITE)
                {
                    m_score [COLOR_WHITE]++;
                }*/
            }
        }
    }


    //test s'il n'y a que de pion d'une seule couleur pour savoir si la personne a gagn� ou pas
    //je le fais ici car le calcul du score vient d'etre fait c'est pratique
    Pligcol->gotoLigCol(1,75);
    std::cout<<"   SCORE";
    Pligcol->gotoLigCol(2,70);
    std::cout << "NOIR : "<<m_score[COLOR_BLACK] << " BLANC : " << m_score[COLOR_WHITE] <<std::endl;

    if(m_score[COLOR_BLACK] == 0)
    {
        m_hasWon = COLOR_WHITE;
        m_isOver = true;
    }
    else if(m_score[COLOR_WHITE] == 0)
    {
        m_hasWon = COLOR_BLACK;

        m_isOver = true;
    }

    delete Pligcol;
}

void Jeu::endGame()
{
    //le joueur a abandonn�
    // system("cls");
    affichagePlateau();

    if (m_score[COLOR_BLACK] == m_score[COLOR_WHITE])  m_egalite = true;
    else if (m_score[COLOR_BLACK] > m_score[COLOR_WHITE]) m_hasWon = COLOR_BLACK;
    else if (m_score[COLOR_BLACK] < m_score[COLOR_WHITE]) m_hasWon = COLOR_WHITE;
   // if (m_score[m_firstPass] == m_score[m_secondPass])
    //  {
    //      m_egalite = true;
    //    std::cout << "LOL";
    //  }
    if(m_hasAbandonned)
    {
        switch(m_hasAbandonned)
        {
        case COLOR_WHITE :
            std::cout << "Le joueur blanc a abandonne !" << std::endl;
            break;
        case COLOR_BLACK :
            std::cout << "Le joueur noir a abandonne !" << std::endl;
            break;
        }
    }
    else if(m_hasWon==false && m_egalite==true)
    {
        std::cout << "Egalite !" << std::endl; //s'il y a �galit�
    }
    else if (m_hasWon) //si qqn n'a plus de jetons sur le terrain ou alors le tableau est rempli et plus personne peut jouer
    {
        switch(m_hasWon)
        {
        case COLOR_WHITE :
            std::cout << "Le joueur blanc est victorieux !" << std::endl;
            break;
        case COLOR_BLACK :
            std::cout << "Le joueur noir est victorieux !" << std::endl;
            break;
        }
    }

    system("pause");

}

void Jeu::afficheCaseJouable ()
{
    //TOUR JOUEUR

    Console* Pligcol = NULL;
    int ligaffichage = 7;

    Pligcol->gotoLigCol(ligaffichage,70);
//    std::cout<<"AVANT CALCUL POSSIBLE JOUEUR";

    for(auto elem : m_coupsjouables)
    {
        std::cout<<"COUPS JOUABLE : lig "<<elem.getLig()+1<<" ; col "<<elem.getCol()+1;
        ligaffichage++;
        Pligcol->gotoLigCol(ligaffichage,70);
    }
    //Pligcol->gotoLigCol(20,70);
    //std::cout<<"APRES CALCUL POSSIBLE JOUEUR";

    delete Pligcol;
}

void Jeu::canPlay(int couleur)
{
    //calcul pour voir si un des deux ne peux pas jouer

    if(!m_coupsjouables.empty())//il n'est pas vide donc remet la variable de sautage de tour � 0
    {
        m_firstPass = 0;
        m_nbpassage = 0;
    }
    else //le vector de coups jouables est vide
    {
        m_firstPass = couleur;
        m_nbpassage ++;

        if( (m_firstPass != 0) && (m_nbpassage == 2)) //si aucun des deux ne peux jouer
        {
            m_secondPass = couleur; //en th�orie l'oppos� de m_firstPass

            //le jeu est finit
            m_isOver = true;
        }
    }

    int nbcasenull = 0;

    for(int i = 0; i < TAILLE; i++)
    {
        for(int j = 0; j < TAILLE; j++)
        {
            if(m_plateau[i][j] == nullptr) nbcasenull ++;
        }
    }

    if(nbcasenull == 0)
    {
        m_isOver = true;
    }
}

void Jeu::caseJouableTab()
{
    Console* Pligcol = NULL;
    int i = 2;
    int j = 5;
    for (auto elem : m_coupsjouables)
    {

        Pligcol -> gotoLigCol (elem.getLig ()*3 +i,elem.getCol ()*6 +j);
        //   if
        Pligcol->setColorMod(15,2);
        std::cout << ".";
        Pligcol -> setColorMod (15,0);
    }

    delete Pligcol;
}

void Jeu::affichemenu ()
{
    Console *pConsole = NULL;
    pConsole -> gotoLigCol (1,15);
    std::cout << "     ___    _________  ____  ____  ________  _____     _____       ___";
    pConsole ->gotoLigCol(2,15);
    std::cout << "   .'   `. |  _   _  ||_   ||   _||_   __  ||_   _|   |_   _|    .'   `.";
    pConsole -> gotoLigCol (3,15);
    std::cout << "  /  .-.  \ |_/ | |  \_|  | |__| |    | |_ \_|   | |       | |     /  .-.  \"";
    pConsole -> gotoLigCol (4,15);
    std::cout << "  | |   | |    | |      |  __  |    |  _| _   | |   _   | |   _ | |   | |";
    pConsole -> gotoLigCol (5,15);
    std::cout << "  \   `-'  /   _| |_    _| |  | |_  _| |__/ | _| |__/ | _| |__/ |\   `-'  /";
    pConsole -> gotoLigCol (6,15);
    std::cout << "   `.___.'   |_____|  |____||____||________||________||________| `.___.'";

    pConsole -> gotoLigCol (11,35);
    std::cout << "Bienvenue dans le jeu de l'othello !\n" << std::endl;
    pConsole -> gotoLigCol (14,35);
    std::cout << "1)Commencer une partie";
    pConsole -> gotoLigCol (15,35);
    std::cout << "2)Charger une partie";
    pConsole -> gotoLigCol (16,35);
    std::cout << "3)Quitter\n" << std::endl;

    delete pConsole;
}
