#include "Arbre.h"

Arbre::Arbre()
{
    //ctor
}

Arbre::~Arbre()
{
    //dtor
//    Noeud* noeudActuel;
//    std::stack<Noeud*> mapile;
//    mapile.push(m_racine);
//    while(!mapile.top()->getEnfants().empty())
//    {
//        noeudActuel = mapile.top();
//        for(auto enfant:noeudActuel->getEnfants())
//        {
//            mapile.push(enfant);
//        }
//    }
//    while(!mapile.empty())
//    {
//        noeudActuel = mapile.top();
//        mapile.pop();
//        noeudActuel->~Noeud();
//    }
}

Arbre::Arbre(Noeud* racine)
      :m_racine(racine)
{

}

void Arbre::creerArbre(int profondeur)
{
    Noeud* noeudActuel=m_racine;
    std::queue<Noeud*> mafile;
    mafile.push(m_racine);
    m_noeuds.push_back(m_racine);
    while(!mafile.empty())
    {
        noeudActuel=mafile.front();
        mafile.pop();
        if(noeudActuel->capableCreerEnfants() && noeudActuel->getProfondeur() != profondeur)
        {
             std::cout<<"profondeur"<<noeudActuel->getProfondeur()<<std::endl;
            noeudActuel->creerenfants();
//<noeudActuel->getCoupDejaJoue()->getLig()
            for(auto enfant:noeudActuel->getEnfants())
            {
                mafile.push(enfant);
                m_noeuds.push_back(enfant);
            }
        }
    }
}

//void Arbre::afficherArbre()
//{
////    int tempPronfondeur;
////    Noeud* noeudActuel;
//    std::cout<<"AFFICHAGE ARBRE, taille arbre = "<<m_noeuds.size()<<std::endl; system("pause");
////    for(auto elem:m_noeuds)
////    {
////        std::cout<<"profondeur : "<<elem->getProfondeur()<<" ; couleur :"<<elem->getCouleur()<<"; casejoue(";
////        std::cout<<elem->getCoupDejaJoue()->getLig()<<";"<<elem->getCoupDejaJoue()->getCol()<<")"<<std::endl;
////    }
//    for(unsigned int i=1;i<m_noeuds.size();i++)
//    {
//        std::cout<<"profondeur : "<<m_noeuds[i]->getProfondeur()<<" ; couleur :"<<m_noeuds[i]->getCouleur()<<"; casejoue(";
//        std::cout<<m_noeuds[i]->getCoupDejaJoue()->getLig()<<";"<<m_noeuds[i]->getCoupDejaJoue()->getCol()<<")"<<"directions size:"<<m_noeuds[i]->getCoupDejaJoue()->getDirections().size()<<std::endl;
//        std::cout<<"LES DIRECTIONS SONT :";
//        for(auto elem:m_noeuds[i]->getCoupDejaJoue()->getDirections())
//        {
//            std::cout<<elem;
//        }
//        std::cout<<std::endl;
//
//    }
////    std::queue<Noeud*> mafile;
////    mafile.push(m_racine);
////    while(!mafile.empty())
////    {
////        noeudActuel = mafile.front();
////        mafile.pop();
//////        if(!noeudActuel->getEnfants().empty())
////    }
//////    while(noeudActuel->getEnfants())
//}

void Arbre::afficherArbre(int lig, int col)
{
//    Console* pconsole = NULL;
////    if(n = getRacine())
////    {
////        pconsole->gotoLigCol(lig,col);
////        std::cout<<"racine";
////    }
//    ///Si le noeud n'a pas deja �t� affich�
//    if(!n->getEstAffiche())
//    {
//        ///Si les enfants ne sont pas affich�s
//        if(!n->getEnfants().empty())
//        {
//            ///Et si les enfants n'ont pas �t� affich�
//            if(!n->getEnfantsSontAffiche())
//            {
//                ///J'appelle afficherArbre pour tout les enfants
//                for(auto enfant:n->getEnfants())
//                {
//
//
//                    afficherArbre(lig+n->getHauteur(),col+15,enfant);
//                    if(!enfant->getEnfants().empty())
//                        std::cout<<"=====>";
//                }
//            }
//        }
//        ///Si le noeud n'a pas d'enfants(au bout de l'arbre)
//        if(n->getEnfants().empty())
//        {
//            pconsole->gotoLigCol(lig,col);
//            int tempLig = lig;
//            int tempCol = col;
//            ///Alors j'affiche tout les frere de cette feuille
//            for(auto frere:n->getPere()->getEnfants())
//            {
//                pconsole->gotoLigCol(tempLig,tempCol);
//                std::cout<<"("<<frere->getCoupDejaJoue()->getLig()<<";"<<frere->getCoupDejaJoue()->getCol()<<")";
//                tempLig++;
//                //incr�mentation de la hauteur du pere
//                n->getPere()->setHauteur(n->getPere()->getHauteur()+1);
//                frere->setEstAffiche(true);
//            }
//            n->getPere()->setHauteur(n->getPere()->getHauteur()+1);
//            n->getPere()->setEnfantsSontAffiche(true);
//        }
//        ///Si tout les enfants du noeud sont affich�s
//        if(n->getEnfantsSontAffiche())
//        {
//            ///j'affiche ce noeud
//            pconsole->gotoLigCol(lig,col);
//            std::cout<<"("<<n->getCoupDejaJoue()->getLig()<<";"<<n->getCoupDejaJoue()->getCol()<<")";
//            n->getPere()->setHauteur(n->getHauteur()+n->getPere()->getHauteur());
//        }
//    }
    calculHauteurs();
    Console* pconsole = NULL;
    std::queue<Noeud*> mafile;
    std::queue<Noeud*> mafile2;
    int profondeur=0;
    Noeud* noeudActuel;
    mafile.push(m_racine);
    mafile2.push(m_racine);
    int tempcol = col;
    int templig = lig;
    while(!mafile2.empty())
    {
        noeudActuel = mafile2.front();
        mafile2.pop();
        for(auto enfant:noeudActuel->getEnfants())
        {
            mafile2.push(enfant);
            mafile.push(enfant);
        }
    }
//    std::cout<<"taille mafile:"<<mafile.size();
//    std::cout<<"taille arbre:"<<m_noeuds.size();
    while(!mafile.empty())
    {
//        std::cout<<"A";
        noeudActuel = mafile.front();
        mafile.pop();
//        if(noeudActuel != m_racine)
//                std::cout<<"p"<<noeudActuel->getProfondeur()<<"("<<noeudActuel->getCoupDejaJoue()->getLig()<<";"<<noeudActuel->getCoupDejaJoue()->getCol()<<")";
//            else std::cout<<"p"<<noeudActuel->getProfondeur()<<"RACINE";
        ///si je passe a la profondeur suivante
        if(noeudActuel->getProfondeur()>profondeur)
        {
            //je passe a la colonne suivante
            tempcol += 15;
            //et je remet la ligne tout en haut
            templig = lig;
            //j'actualise la profondeur actuelle
            profondeur=noeudActuel->getProfondeur();
            //j'affiche le noeud
            pconsole->gotoLigCol(templig,tempcol);
            if(noeudActuel != m_racine)
            {
                if(noeudActuel->getEstMeilleur()) pconsole->setColor(COLOR_RED);
                std::cout<<"-->("<<noeudActuel->getCoupDejaJoue()->getLig()+1<<";"<<noeudActuel->getCoupDejaJoue()->getCol()+1<<")";
                pconsole->setColor(COLOR_BLUE);
                std::cout<<noeudActuel->getValeur();
                pconsole->setColor(COLOR_DEFAULT);
            }

            else std::cout<<"RACINE";
            templig += noeudActuel->getHauteur();

        }
        ///sinon
        else
        {
            //j'affiche le noeud
            pconsole->gotoLigCol(templig,tempcol);
            if(noeudActuel != m_racine)
            {
                if(noeudActuel->getEstMeilleur()) pconsole->setColor(COLOR_RED);
                std::cout<<"-->("<<noeudActuel->getCoupDejaJoue()->getLig()+1<<";"<<noeudActuel->getCoupDejaJoue()->getCol()+1<<")";
                pconsole->setColor(COLOR_BLUE);
                std::cout<<noeudActuel->getValeur();
                pconsole->setColor(COLOR_DEFAULT);
                templig += noeudActuel->getHauteur();
            }

            else
            {
                if(noeudActuel->getEstMeilleur()) pconsole->setColor(COLOR_RED);
                std::cout<<"RACINE-->";
                pconsole->setColor(COLOR_BLUE);
                std::cout<<noeudActuel->getValeur();
                pconsole->setColor(COLOR_DEFAULT);
            }
            //puis je bouge le curseur d'une distance de la hauteur du noeudactuel

        }
//        getch();

    }
}


void Arbre::calculHauteurs()
{
//    if(!noeudActuel->getEnfants().empty())
//    {
//        if(noeudActuel->getEnfants()[0]->getEnfants().empty())
//        {
//            noeudActuel->setHauteur(noeudActuel->getEnfants().size() +1);
//            return noeudActuel->getHauteur();
//        }
//       else
//        {
//            for(auto enfant:noeudActuel->getEnfants())
//            {
//                noeudActuel->setHauteur(noeudActuel->getHauteur()+calculHauteurs(enfant));
//            }
//        }
//    }
//    else
//    {
//        noeudActuel->setHauteur(noeudActuel->getHauteur()+1);
//    }

    Noeud* noeudActuel;
    std::stack<Noeud*> mapile;
    std::queue<Noeud*> mafile;
    mapile.push(m_racine);
    mafile.push(m_racine);
    while(!mafile.empty())
    {
        noeudActuel = mafile.front();
        mafile.pop();
        mapile.push(noeudActuel);
        for(auto enfant:noeudActuel->getEnfants())
        {
            mafile.push(enfant);
//            std::cout<<"A";
        }
    }
//    std::cout<<"TAILLE PILE :"<<mapile.size();
    while(!mapile.empty())
    {
        noeudActuel = mapile.top();
        mapile.pop();
//        std::cout<<"("<<noeudActuel->getCoupDejaJoue()->getLig()<<";"<<noeudActuel->getCoupDejaJoue()->getCol()<<")"<<std::endl;
        if(noeudActuel->getEnfants().empty())
        {
//            std::cout<<"PAS ENFANTS"<<std::endl;

            //si c'est le dernier enfant
            if(noeudActuel == noeudActuel->getPere()->getEnfants().back())  noeudActuel->setHauteur(2);
            //sinon
            else noeudActuel->setHauteur(1);
        }
        else
        {
//            std::cout<<"ENFANT"<<std::endl;
            int temphauteur =0;
            for(auto enfant:noeudActuel->getEnfants())
            {
                temphauteur+=enfant->getHauteur();
            }
            noeudActuel->setHauteur(temphauteur);
        }



    }
}

