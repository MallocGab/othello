#include "CaseJouable.h"

CaseJouable::CaseJouable()
{
    //ctor
}

CaseJouable::CaseJouable(int lig, int col, std::vector<int> directions)
{
    //ctor
    if(lig>=0 && lig<=TAILLE) m_lig=lig;
    else std::cout<<"ERREUR : LIG DOIT ETRE ENTRE 0 ET 8"<<std::endl;
    if(col>=0 && col<=TAILLE) m_col=col;
    else std::cout<<"ERREUR : COL DOIT ETRE ENTRE 0 ET 8"<<std::endl;
    m_directionspossibles = directions;
}

CaseJouable::CaseJouable(CaseJouable* p)
{
    m_lig = p->getLig();
    m_col = p->getCol();
    m_directionspossibles = p->getDirections();
 //   m_parent = p->getParent();
}

CaseJouable::CaseJouable(CaseJouable const &copie)
{
    m_lig=copie.getLig();
    m_col=copie.getCol();
    m_directionspossibles = copie.getDirections();
 //   m_parent = copie.getParent();
}
void CaseJouable::addDirection(int d)
{
    if(d!=8 || d!=9 || d!=6 || d!=3 || d!=2 || d!=1 || d!=4 || d!=7)
    {
        std::cout<<"ERREUR SUR LA VALEUR DE LA DIRECITON"<<std::endl;
    }
    else
    {
        m_directionspossibles.push_back(d);
    }
}

CaseJouable::~CaseJouable()
{
    //dtor

    //A NE PAS DECOMMENTER
    //delete m_parent;
}
