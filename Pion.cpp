#include "Pion.h"

Pion::Pion()
{
    //ctor
}

Pion::Pion(int couleur)
    : m_couleur(couleur)
{
    if (m_couleur == COLOR_WHITE) m_symbole = 'B';
    else if (m_couleur == COLOR_BLACK) m_symbole = 'N';
}

Pion::~Pion()
{
    //dtor
}
