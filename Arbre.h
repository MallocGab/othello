#ifndef ARBRE_H
#define ARBRE_H
#include <stack>
#include <queue>
#include <vector>
#include "Noeud.h"
#include "console.h"

class Arbre
{
    public:
        Arbre();
        Arbre(Noeud* racine);
        virtual ~Arbre();
        void afficherArbre(int lig,int col);
        void creerArbre(int profondeur);
        void calculHauteurs();

        ///Getters
        Noeud* getRacine() {return m_racine;}
        std::vector<Noeud*> getNoeuds() {return m_noeuds;}
        ///Setters
        void setRacine(Noeud* r){m_racine =r;}
        void setNoeuds(std::vector<Noeud*> noeuds) {m_noeuds = noeuds;}


    private:
        Noeud* m_racine;
        std::vector<Noeud*> m_noeuds;
};

#endif // ARBRE_H
