#ifndef CASEJOUABLE_H
#define CASEJOUABLE_H
#include <vector>
#include <iostream>

#define TAILLE 8

class CaseJouable
{
    public:

    bool operator <(const CaseJouable & a) const
    {
        return ( m_lig < a.getLig() );
    }


        CaseJouable();
        CaseJouable(int lig, int col, std::vector<int> directions);
        CaseJouable(CaseJouable* pointeur);
        CaseJouable(CaseJouable const& copie);
        virtual ~CaseJouable();

        //getters
        int getLig() const {return m_lig;}
        int getCol() const {return m_col;}
        std::vector<int> getDirections() const {return m_directionspossibles;}
  //      CaseJouable* getParent() const {return m_parent;}

        //setters
        void setLig(int lig) {m_lig = lig;}
        void setCol(int col) {m_col = col;}
        void setDirections(std::vector<int> dirs) {m_directionspossibles = dirs;}
//        void setParent (CaseJouable* parent) {m_parent = parent;}

        void addDirection(int d);

    protected:

    private:
        int m_lig; //ligne
        int m_col; // colonne
        std::vector<int> m_directionspossibles;

     //   CaseJouable* m_parent;
};

#endif // CASEJOUABLE_H
