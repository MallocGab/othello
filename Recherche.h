#ifndef RECHERCHE_H
#define RECHERCHE_H

#include "Pion.h"
#include "CaseJouable.h"
#include <vector>


class Recherche
{
    public:
        Recherche();
        Recherche(int couleur, std::vector<std::vector<Pion*>> plateau);
        Recherche(int couleur, std::vector<CaseJouable> coupsJouables, std::vector<std::vector<Pion*>> plateau);
        ~Recherche();

        int getCouleur() { return m_couleur; }
        void setCouleur(int val) { m_couleur = val; }
        int getCouleurAdversaire() { return m_couleurAdversaire; }
        void setCouleurAdversaire(int val) { m_couleurAdversaire = val; }
        std::vector<CaseJouable> getCoupsjouables() { return m_coupsjouables; }
        void setCoupsjouables(std::vector<CaseJouable> val) { m_coupsjouables = val; }
        std::vector<std::vector<Pion*>> getPlateau() { return m_plateau; }
        void setPlateau(std::vector<std::vector<Pion*>> val) { m_plateau = val; }

        //fonctions de Francois
        //retourne si la case est jouable ou non
        //si la case est jouable, il l'ajoute a m_coupsjouables
        bool isCaseJouable(int lig, int col,int couleur);
        void calculdescoupjouables(int couleur);
        bool verifJouabiliteDirection(int lig, int col, int coulJoueur,int d);

        bool calcIsInTab(int lig, int col);
        void calcNewCoord(int &lig, int& col,int d);


        //fonction ajout (de Francois)
        int ajouterPion(int lig, int col,int couleur, std::vector<std::vector<Pion*>>& plateauJeu);


        //fonctions de construction g�n�rale
        //void rechercheCoupsPossible();

    protected:

    private:
        int m_couleur;
        int m_couleurAdversaire = 0;
        std::vector<CaseJouable> m_coupsjouables;
        std::vector<std::vector<Pion*>> m_plateau;
};

#endif // RECHERCHE_H
