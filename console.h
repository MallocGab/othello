#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include <iostream>
#include <conio.h>
//#include <allegro.h>
//#include <winalleg.h>
#include <windows.h>
#include <fstream>
#include <vector>
//#include "main.h"

/*
    Code de Monsieur Diedler donn� au premier semestre pour le projet SIAM
*/

enum Color
{
    COLOR_RED = 0,
    COLOR_WHITE = 1,
    COLOR_BLACK = 2,
    COLOR_GREEN = 3,
    COLOR_BLUE = 4,
    COLOR_YELLOW = 5,
    COLOR_PURPLE = 6,
    COLOR_DEFAULT = 7, // gris couleur de base de la console
};

class Console
{
    private:
        // Empecher la cr�ation


        // Empecher la copie d'objet...
        Console& operator= (const Console&){ return *this;}
        Console (const Console&){}

        // Attributs
        static Console* m_instance;

        // M�thodes priv�es
        void _setColor(int front, int back);
    public:
        Console();
        ~Console();
        // M�thodes statiques (publiques)
        static Console* getInstance();
        static void deleteInstance();

        // M�thodes publiques
        void gotoLigCol(int lig, int col);
        bool isKeyboardPressed();
        int getInputKey();
        void setColor(Color col);
        void setColorMod(int f,int b);
        void colorierCase(int y, int x, int f, int b);
};

#endif // CONSOLE_H_INCLUDED

