#ifndef PION_H
#define PION_H

#include <iostream>
#include "console.h"

class Pion
{
    public:
        Pion();
        Pion(int couleur);
        ~Pion();

        int getCouleur() { return m_couleur; }
        void setCouleur(int val) { m_couleur = val; }
        char getSymbole() { return m_symbole; }
        void setSymbole(char val) { m_symbole = val; }

    protected:

    private:
        int m_couleur; //1 = blanc, 2 = noir, on utilisera plutot COLOR_BLACK et COLOR_WHITE d�finit dans l'enum de console.h
        char m_symbole;
};

#endif // PION_H
