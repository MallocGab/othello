#include "Noeud.h"

Noeud::Noeud()
{
    //ctor
}

Noeud::~Noeud()
{
    //dtor
//    for(int i=0;i<8;i++)
//    {
//        for(int j=0;j<8;j++)
//        {
//            delete m_plateau[i][j];
//        }
//    }
    for(unsigned int i=0;i<m_enfants.size();i++)
    {
        delete m_enfants[i];
    }
    delete m_coupdejajoue;
}

Noeud::Noeud(std::vector<std::vector<Pion*>> plateau, int profondeur, int couleur)
    :m_plateau(plateau),m_profondeur(profondeur),m_couleur(couleur)
{
    m_valeur = 0;
    m_nbPionRetourne = 0;
}

Noeud::Noeud(Noeud* papa, std::vector<std::vector<Pion*>> plateau, int profondeur, int couleur, CaseJouable* coupDejaJoue, int nbpionretourne)
    :m_pere(papa),m_plateau(plateau),m_profondeur(profondeur),m_couleur(couleur),m_nbPionRetourne(nbpionretourne)
{
    m_valeur = 0;
    m_coupdejajoue = new CaseJouable(coupDejaJoue);
}

void copieplateau(std::vector<std::vector<Pion*>>& copie, std::vector<std::vector<Pion*>>& origine)
{
    copie.resize(8, std::vector<Pion*>(1 << 7,0));

    for(int i = 0; i<8; i++)
    {
        for(int j = 0; j<8; j++)
        {
            if(origine[i][j] != nullptr)
            {
                copie[i][j] = new Pion(origine[i][j]->getCouleur());
            }
            else copie[i][j] = nullptr;

        }
    }

}

void Noeud::creerenfants()
{
    //initialisation de l'atat initial, avec en meme temps calcul des coups jouable a cet etat
    Recherche etatinit(m_couleur, m_plateau);
    int tempNbPionRetourne = 0;
    for(auto coup:etatinit.getCoupsjouables())
    {
        std::vector<std::vector<Pion*>> temp_plateau;
        temp_plateau.resize(TAILLE, std::vector<Pion*>(1 << (TAILLE-1),0));
        copieplateau(temp_plateau,m_plateau);
        Recherche etatenfant(m_couleur,temp_plateau);
        tempNbPionRetourne = etatenfant.ajouterPion(coup.getLig(),coup.getCol(),m_couleur,temp_plateau);
//        std::cout<<"coupjoue("<<coup.getLig()<<";"<<coup.getCol()<<")"<<std::endl;
        if(m_couleur == COLOR_WHITE)    m_enfants.push_back(new Noeud(this,temp_plateau,m_profondeur+1,COLOR_BLACK,new CaseJouable(coup),tempNbPionRetourne));
        if(m_couleur == COLOR_BLACK)    m_enfants.push_back(new Noeud(this,temp_plateau,m_profondeur+1,COLOR_WHITE,new CaseJouable(coup),tempNbPionRetourne));
    }
}

bool Noeud::capableCreerEnfants()
{
    Recherche etatinit(m_couleur, m_plateau);
    if(etatinit.getCoupsjouables().empty()) return false;
    else return true;
}

