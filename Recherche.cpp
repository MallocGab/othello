#include "Recherche.h"

Recherche::Recherche()
{
    //ctor
}

Recherche::~Recherche()
{
    //dtor

    // A NE PAS DECOMMENTER
    /*for(const auto & elem1 : m_plateau)
    {
        for(const auto & elem2 : elem1)
        {
            delete elem2;
        }
    }*/
}


//constructeurs surcharg�s
Recherche::Recherche(int couleur, std::vector<std::vector<Pion*>> plateau)
    :m_couleur(couleur), m_plateau(plateau)
{
    if(couleur == COLOR_BLACK) m_couleurAdversaire == COLOR_WHITE;
    if(couleur == COLOR_WHITE) m_couleurAdversaire == COLOR_BLACK;
    //appel automatiquement la fonction afinde cr�er le tableau de coups possibles
    calculdescoupjouables(couleur);
}


Recherche::Recherche(int couleur, std::vector<CaseJouable> coupsJouables, std::vector<std::vector<Pion*>> plateau)
    :m_couleur(couleur), m_coupsjouables(coupsJouables), m_plateau(plateau)
{
    if(couleur == COLOR_BLACK) m_couleurAdversaire == COLOR_WHITE;
    if(couleur == COLOR_WHITE) m_couleurAdversaire == COLOR_BLACK;

  //nada on attend juste l'ajout d'un pion puisqu'on a d�j� le tableau de coups jouables
}


void Recherche::calculdescoupjouables(int couleur)
{
    for(int lig=0; lig<TAILLE; lig++)
    {
        for(int col=0; col<TAILLE; col++)
        {
//            std::cout << "Pour la case : " << lig << " " << col << std::endl;
            isCaseJouable(lig,col,couleur);

        }
    }
}

bool Recherche::isCaseJouable(int lig, int col,int couleur)
{
    bool d8,d9,d6,d3,d2,d1,d4,d7 = false;
    bool jouable = false;
    std::vector<int> directionsjouables;
    ///regarde dans les 8 directions si c'est adjacent a un pion
    if(m_plateau[lig][col] == nullptr)
    {
//        std::cout << "directions" << std::endl;
        d8 = verifJouabiliteDirection(lig,col,couleur,8);
        d9 = verifJouabiliteDirection(lig,col,couleur,9);
        d6 = verifJouabiliteDirection(lig,col,couleur,6);
        d3 = verifJouabiliteDirection(lig,col,couleur,3);
        d2 = verifJouabiliteDirection(lig,col,couleur,2);
        d1 = verifJouabiliteDirection(lig,col,couleur,1);
        d4 = verifJouabiliteDirection(lig,col,couleur,4);
        d7 = verifJouabiliteDirection(lig,col,couleur,7);

        jouable = (d8 || d9 || d6 || d3 || d2 || d1 || d4 || d7);
        if(d8) directionsjouables.push_back(8);
        if(d9) directionsjouables.push_back(9);
        if(d6) directionsjouables.push_back(6);
        if(d3) directionsjouables.push_back(3);
        if(d2) directionsjouables.push_back(2);
        if(d1) directionsjouables.push_back(1);
        if(d4) directionsjouables.push_back(4);
        if(d7) directionsjouables.push_back(7);


    }
    if(jouable)
    {
//        std::cout << "c'est jouable" << std::endl;
        CaseJouable cj(lig,col,directionsjouables);
        m_coupsjouables.push_back(cj);

    }
    return jouable;
    // si les adj sont pas de sa couleur
    // pour chaque dir, aller plus loin jusqu'a trouver la couleur pareil ou n'avoir plus de pion ou etre a la limite
    // si je trouve coul pareil, possible = oui
    // si la direction est bonne, il faut l'enregistrer et peut etre la metter dans une classe coups jouables

    //return possible;

}

bool Recherche::verifJouabiliteDirection(int lig, int col, int coulJoueur,int d)
{
    ///Initialisaiton variables
    int i = lig;
    int j = col;
    bool isCaseVide = true;
    bool isInTab = true;
    bool jouable = false;
    ///Determination des coordonn�ees en fonction de la direction de verif
    calcNewCoord(i,j,d);
    ///Si la case adjacente  est dans le tableau..
    if(calcIsInTab(i,j))
    {
        ///..Et si elle n'est pas vide..
        if(m_plateau[i][j] != nullptr)
        {
            ///..ET Si la couleur est differente de celle du joueur..
            if(m_plateau[i][j]->getCouleur() != coulJoueur)
            {
                ///Boucle de verification de chaque case jusqu'a avoir la couleur de celle du joueur
                /// ou jusqu'a sortir du tableau ou n'avoir plus de pion sur la case
                do
                {
                    //je passe a la case suivante
                    calcNewCoord(i,j,d);
                    isInTab = calcIsInTab(i,j);
                    //si je suis toujours dans le tableau
                    if(isInTab)
                    {
                        isCaseVide = (m_plateau[i][j] == nullptr);
                        // et si il y a un pion
                        if(m_plateau[i][j] != nullptr)
                        {

                            // alors je regarde la couleur de la case
                            // si la couleur de la case est pareil que celle du joueur alors c'est jouable
                            if(m_plateau[i][j]->getCouleur() == coulJoueur)
                            {
                                jouable = true;
                            }
                        }
                    }

                }
                while(!jouable && isInTab && !isCaseVide); //le probleme semble entre sur iscasevide
            }
        }
    }

    return jouable;
}

void Recherche::calcNewCoord(int &lig,int &col, int d)
{
    switch(d)
    {
    case 8 :
        lig-=1;
        break;
    case 9 :
        lig-=1;
        col+=1;
        break;
    case 6 :
        col+=1;
        break;
    case 3 :
        lig+=1;
        col+=1;
        break;
    case 2 :
        lig+=1;
        break;
    case 1 :
        lig+=1;
        col-=1;
        break;
    case 4 :
        col-=1;
        break;
    case 7 :
        lig-=1;
        col-=1;
        break;
    }
}

bool Recherche::calcIsInTab(int lig, int col)
{
    return (lig>=0 && lig<TAILLE && col>=0 && col<TAILLE);
}


int Recherche::ajouterPion(int lig,int col,int couleur, std::vector<std::vector<Pion*>>& plateauJeu)
{
    ///regarde dans quel direction il y a coul differente
    /// Initialisation des variables
    bool jouable = false; //pas sur que cette variable soit utile
    bool finboucle = false;
    int nbPionRetourne = 0;
    int i,j;
    std::vector<int> directions;
    ///Recup�re les directions a voir pour le retournement des pions
    for(auto elem:m_coupsjouables)
    {
        //trouve si le coup est jouable
        if(elem.getLig() == lig && elem.getCol() == col)
        {
            jouable = true;
            //si jouable, alors je recupere les directions vers lesquel regarder
            directions = elem.getDirections();
        }
    }
    if(jouable)
    {
        plateauJeu[lig][col] = new Pion(couleur);
        ///Boucle de changement de couleurs des pions pour chaque directions possibles
        for(const auto& elem:directions)
        {
            //init variables
            i=lig;
            j=col;
            finboucle = false;
            //boucle de retournement pour une direction
            while(finboucle == false)
            {
                //calcul des nouvelles coords en fct de la direction
                calcNewCoord(i,j,elem);
                if(calcIsInTab(i,j))
                {
                    if(plateauJeu[i][j] != nullptr)
                    {
                        //si la couleur du pion est differente de celle du joueur...
                        if(plateauJeu[i][j]->getCouleur() != couleur)
                        {
                            //... alors je change la couleur du pion
                            plateauJeu[i][j]->setCouleur(couleur);
                            if(couleur == COLOR_WHITE )plateauJeu[i][j]->setSymbole('B');
                            if(couleur == COLOR_BLACK )plateauJeu[i][j]->setSymbole('N');
                            nbPionRetourne ++;
                        }
                        //... sinon je suis tomb� sur le pion du joueur situ� � l'extr�mit� du parcours
                        // je met donc fin a la boucle de retournement des pions
                        else
                        {
                            finboucle = true;
                        }
                    }
                }
            }
        }
    }

    return nbPionRetourne;
}
