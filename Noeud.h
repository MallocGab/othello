#ifndef NOEUD_H
#define NOEUD_H
#include <vector>
#include "Pion.h"
#include "CaseJouable.h"
#include "Recherche.h"

class Noeud
{
    public:
        Noeud();
        Noeud(std::vector<std::vector<Pion*>> plateau, int profondeur, int couleur);
        Noeud(Noeud* papa, std::vector<std::vector<Pion*>> plateau, int profondeur, int couleur, CaseJouable* coupDejaJoue,int nbpionretourne);
        ~Noeud();

        ///Fonctions
        void creerenfants();
        bool capableCreerEnfants();

        ///Getters
        Noeud* getPere(){return m_pere;}
        std::vector<Noeud*> getEnfants(){return m_enfants;}
        int getProfondeur(){return m_profondeur;}
        int getValeur(){return m_valeur;}
        int getCouleur(){return m_couleur;}
        CaseJouable* getCoupDejaJoue() {return m_coupdejajoue;}
        int getNbPionRetourne() {return m_nbPionRetourne; }
        int getHauteur() {return m_hauteur;}
        bool getEstAffiche() {return m_estAffiche;}
        bool getEnfantsSontAffiche() {return m_enfantsSontAffiche;}
        int getHauteurFreres() {return m_hauteurFreres;}
        bool getEstMeilleur() {return m_estMeilleur;}

        ///Setters
        void setPere(Noeud* pere){m_pere = pere;}
        void setEnfants(std::vector<Noeud*> enfants){m_enfants = enfants;}
        void setProfondeur(int profondeur){m_profondeur = profondeur;}
        void setValeur(int valeur){m_valeur = valeur;}
        void setCouleur(int couleur){m_couleur = couleur;}
        void setCoupDejaJoue(CaseJouable* coup){m_coupdejajoue = coup;}
        void setNbPionRetourne(int val){m_nbPionRetourne = val;}
        void setHauteur(int h) {m_hauteur = h;}
        void setEstAffiche(bool affiche) {m_estAffiche = affiche;}
        void setEnfantsSontAffiche(bool affich) {m_enfantsSontAffiche = affich;}
        void setHauteurFreres(int hf) {m_hauteurFreres = hf;}
        void setEstMeilleur(bool em) {m_estMeilleur = em;}


    private:
        Noeud* m_pere = nullptr; // pointeur sur le pere
        std::vector<Noeud*> m_enfants; //vecteur des descendants
        std::vector<std::vector<Pion*>> m_plateau; //etat du plateau a ce noeud apres que le coup jouable soit jou�
        int m_profondeur; //profondeur actuel du noeud
        int m_valeur; // la valeur du coup du noeud, on le calcule grace a l'heuristique
        int m_couleur; // couleur du joueur dans a cette profondeur
        CaseJouable* m_coupdejajoue = nullptr; //
        int m_nbPionRetourne;
        int m_hauteur = 0;
        int m_hauteurFreres =0;
        bool m_estAffiche = false;
        bool m_enfantsSontAffiche = false;
        bool m_estMeilleur = false;
};

#endif // NOEUD_H
