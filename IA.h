#ifndef IA_H
#define IA_H

#include <cstdlib>
#include <ctime>
#include <climits>
#include "CaseJouable.h"
#include "Pion.h"
#include "Jeu.h"
#include "Recherche.h"
#include "Noeud.h"
#include "Arbre.h"
#include <map>
#include <unordered_map>

class IA
{
    public:
        IA();
        IA(int couleur);
        ~IA();

        //getters et setters
        int getMax() {return m_max; }
        void setMax(int val) {m_max = val;}
        unsigned int getScore() { return m_score; }
        void setScore(unsigned int val) { m_score = val; }
        int getCouleurIA() { return m_couleurIA; }
        void setCouleurIA(int val)
        {
            if(val == COLOR_BLACK){
                m_couleurIA = val;
                m_couleurAdversaire = COLOR_WHITE;
            }
            else if(val == COLOR_WHITE)
            {
                m_couleurIA = val;
                m_couleurAdversaire = COLOR_BLACK;
            }
        }

        //int getForce() {return m_force;}
        //void setForce(std::vector<int> val) {m_force = val;}



        //fonctions
        CaseJouable aleatoire(std::vector<CaseJouable>& CoupsJouable);
        //CaseJouable minMax(std::vector<CaseJouable> CoupsJouable, std::vector<std::vector<Pion*>> plateau, int profondeur);

        //source : http://web.archive.org/web/20070820195815/http://www.seanet.com:80/~brucemo/topics/minmax.htm

        int Min(Noeud* noeud, int profondeur);
        int Max(Noeud* noeud, int profondeur);

        int negaMax(Noeud* noeud, int profondeur);

        CaseJouable minMax2(std::vector<std::vector<Pion*>> plateau,int profondeur,bool afficherArbre);
        CaseJouable appelNegaMAx(std::vector<std::vector<Pion*>> plateau,int profondeur,bool afficherArbre);

        int eval(Noeud* noeud);
        // avant : int eval(CaseJouable caseTest,std::vector<std::vector<Pion*>>& plateauEtat );
        // Copie les valeurs des pions point�s, cr�er des new Pion, met les valeurs dedans, et mes ces newpions dans copie
        void copieplateau(std::vector<std::vector<Pion*>>& copie, std::vector<std::vector<Pion*>>& origine);


    protected:

    private:

        bool calcIsInTab(int lig, int col);
        void calcNewCoord(int &lig,int &col, int d);
        void initForce();

        unsigned int m_score;
        int m_couleurIA;
        int m_couleurAdversaire;

        //useless
        int m_max = 0;
        int m_min = 64;
        int m_maxAdv = 0;

        int m_value = 0;
        CaseJouable* m_caseValue = nullptr;
        int m_tour = 0;
     //   int m_profondeurOriginal;

        CaseJouable m_CaseOuJouer;

        /*CaseJouable m_CaseSelect;
        std::vector<std::vector<Pion*>> m_PlateauSelect;

        std::map<CaseJouable, std::vector<std::vector<Pion*>>> m_possibiliteTableau;
        std::map<CaseJouable, int> m_possibiliteCompteur;*/

        std::map<int, CaseJouable*> m_sauv;
   //     std::unordered_map<CaseJouable*> m_unorderedsauv;

        int m_force[8][8];

};

#endif // IA_H
