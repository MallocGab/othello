#include "jeu.h"

using namespace std;

int main()
{
    Jeu jeu;
    bool choixFait = false;
    int choix;

    while(!choixFait)
    {
        int retour = jeu.menu();

        if (retour != 1)
        {
            system("cls");

            cout << "Que souhaitez vous faire ?\n1)Recommencer\n2)Quitter" << std::endl;
            cin >> choix;
            switch(choix)
            {
            case 1: system("cls");
                break;
            case 2: choixFait = true;
                break;
            default :  cout <<"Saisie invalide veuiller resaissir votre choix : ";
                cin >> choix;
                break;
            }

        }
        else choixFait = true;
    }

    return 0;
}

//END_OF_MAIN();
