#include "IA.h"

IA::IA()
{
    //ctor
}

IA::~IA()
{
    //dtor

    for(const auto & elem : m_sauv)
    {
        delete elem.second;
    }
}

IA::IA(int couleur)
    :m_couleurIA(couleur)
{
    if(couleur == COLOR_WHITE) m_couleurAdversaire = COLOR_BLACK;
    else if (couleur == COLOR_BLACK) m_couleurAdversaire = COLOR_WHITE;

    m_score = 0;
    m_caseValue = nullptr;

    for(int i = 0; i < TAILLE; i++)
    {
        for (int j = 0; j < TAILLE; j++)
        {
            m_force[i][j] = 0;
        }
    }
}

CaseJouable IA::aleatoire(std::vector<CaseJouable> &CoupsJouable)
{
    //prend un chiffre al�atoire entre 0 et la taille du tableau jouable
    int sizeCoupsJouable = CoupsJouable.size();

    //source : http://stackoverflow.com/questions/13445688/how-to-generate-a-random-number-in-c
    srand(time(NULL));
    int random_number = std::rand()%sizeCoupsJouable;

    return CoupsJouable[random_number];
}

//PREMIER JET DE MIN MAX N'AYANT JAMAIS FONCIONNE

//
//CaseJouable IA::minMax(std::vector<CaseJouable> CoupsJouableParIA,std::vector<std::vector<Pion*>> plateau,int profondeur)
//{
////    int valIA; //valeur case IA
////    int valAdv; //valeur case Adversaire
////    int vallProf; //valeur case IA en profondeur (� changer en vector si profnodeur > 1 ?)
//
//    Console* Pligcol = NULL;
//
//    std::vector<int> minIA;
//    std::vector<int> maxIA;
//
//    CaseJouable CaseMin;
//    std::vector<std::vector<Pion*>> TabMin;
//
//    CaseJouable CaseMax;
//    std::vector<std::vector<Pion*>> TabMax;
//
//    CaseJouable CaseMaxAdv;
//    std::vector<std::vector<Pion*>> TabMaxAdv;
//
//    if(profondeur == 0)
//    {
//        return m_CaseOuJouer;
//    }
//    else
//    {
//        std::vector<std::vector<Pion*>> copieplat;
//        std::vector<std::vector<Pion*>> copieplat2;
//        std::vector<std::vector<Pion*>> copieplat3;
//
//        Recherche CoupsJouable(m_couleurIA, CoupsJouableParIA, plateau);
//
//        m_max = 0;
//
//        std::cout << "Voici le tableau original : " << std::endl;
//        for(int i = 0; i < TAILLE; i++)
//        {
//            for(int j = 0; j < TAILLE; j++)
//            {
//                if(plateau[i][j] != nullptr)
//                {
//                    std::cout << plateau[i][j]->getSymbole();
//                }
//                else std::cout << ".";
//            }
//            std::cout << std::endl;
//        }
//
//        /*
//        *   COUPS JOUABLES PAR IA A L ORIGIN
//        */
//
//        std::cout << "L'ensemble des coups jouables par IA est a l'origine :" << std::endl;
//        for(auto& elemIA : CoupsJouableParIA)
//        {
//            std::cout << "lig : " << elemIA.getLig()+1 << " col : " << elemIA.getCol()+1 << std::endl;
//        }
//
//        system("pause");
//
//        for(auto& elemIA : CoupsJouableParIA)
//        {
//            copieplateau(m_possibiliteTableau[elemIA],plateau);
//
//            std::cout << "Le coup IA origine selectionne est : " << elemIA.getLig()+1 << " " << elemIA.getCol()+1 << std::endl;
//
//            CoupsJouable.ajouterPion(elemIA.getLig(), elemIA.getCol(), m_couleurIA, m_possibiliteTableau[elemIA]);
//
//            for(int i = 0; i < TAILLE; i++)
//            {
//                for(int j = 0; j < TAILLE; j++)
//                {
//                    if(m_possibiliteTableau[elemIA][i][j] != nullptr)
//                    {
//                        std::cout << m_possibiliteTableau[elemIA][i][j]->getSymbole();
//                    }
//                    else std::cout << ".";
//                }
//                std::cout << std::endl;
//            }
//
//            Recherche CoupsAdversaire(m_couleurAdversaire, m_possibiliteTableau[elemIA]);
//            std::vector<CaseJouable> coupsPossibleAdversaire = CoupsAdversaire.getCoupsjouables();
//
//            std::cout << coupsPossibleAdversaire.size() << std::endl;
//
//            std::cout << "L'ensemble des coups possibles pour l'adversaire dans cette configuration est : " << std::endl;
//            for(const auto & elem : coupsPossibleAdversaire)
//            {
//                std::cout << elem.getLig()+1 << " " << elem.getCol()+1 << std::endl;
//            }
//
//
//            m_maxAdv =0;
//
//            /*
//            *   COUP JOUABLE PAR L ADVERSAIRE
//            */
//
//            system("pause");
//
//            for(auto& elemAdv : coupsPossibleAdversaire)
//            {
//                copieplateau(m_possibiliteTableau[elemAdv],m_possibiliteTableau[elemIA]);
//
//                elemAdv.setParent(&elemIA);
//
//                std::cout << "Le coup adversaire selectionne est : " << elemAdv.getLig()+1 << " " << elemAdv.getCol()+1 << std::endl;
//
//                CoupsAdversaire.ajouterPion(elemAdv.getLig(), elemAdv.getCol(), m_couleurAdversaire, m_possibiliteTableau[elemAdv]);
//
//                for(int i = 0; i < TAILLE; i++)
//                {
//                    for(int j = 0; j < TAILLE; j++)
//                    {
//                        if(m_possibiliteTableau[elemAdv][i][j] != nullptr)
//                        {
//                            std::cout << m_possibiliteTableau[elemAdv][i][j]->getSymbole();
//                        }
//                        else std::cout << ".";
//                    }
//                    std::cout << std::endl;
//                }
//
//                Recherche CoupsProfondeur(m_couleurIA, m_possibiliteTableau[elemAdv]);
//                std::vector<CaseJouable> coupsPossibleProfondeur = CoupsProfondeur.getCoupsjouables();
//
//                m_min = 64; //remise � 64 de m_min
//
//                /*
//                *   COUP JOUABLE PAR IA EN PROFONDEUR
//                */
//
//                std::cout << "L'ensemble des coups en profondeur par l'IA est :" << std::endl;
//                for(const auto & elem : coupsPossibleProfondeur)
//                {
//                    std::cout << elem.getLig()+1 << " " << elem.getCol()+1 << std::endl;
//                }
//
//                system("pause");
//
//                for(auto& elemProf : coupsPossibleProfondeur)
//                {
//                    copieplateau(m_possibiliteTableau[elemProf],m_possibiliteTableau[elemAdv]);
//
//                    std::cout << "Le coup en profondeur pour l'ia selectionne est : " << elemProf.getLig()+1 << " " << elemProf.getCol()+1 << std::endl;
//
//                    elemProf.setParent(&elemAdv);
//
//                    //CoupsProfondeur.ajouterPion(elemProf.getLig(),elemProf.getCol(), m_couleurIA, m_possibiliteTableau[elemProf]); //car on le fait dans eval en fait
//                    int valProf = eval(elemProf, m_possibiliteTableau[elemProf]);
//
//                    for(int i = 0; i < TAILLE; i++)
//                    {
//                        for(int j = 0; j < TAILLE; j++)
//                        {
//                            if(m_possibiliteTableau[elemProf][i][j] != nullptr)
//                            {
//                                std::cout << m_possibiliteTableau[elemProf][i][j]->getSymbole();
//                            }
//                            else std::cout << ".";
//                        }
//                        std::cout << std::endl;
//                    }
//
//
//                    std::cout << "valProf : " << valProf << std::endl;
//                    std::cout << "m_min : " << m_min << std::endl;
//
//                    if(valProf < m_min)
//                    {
//                        m_min = valProf;
//                        CaseMin = elemProf;
//                        TabMin = m_possibiliteTableau[elemProf];
//
//                        std::cout << "WOW TabMin est :" << std::endl;
//                        for(int i = 0; i < TAILLE; i++)
//                        {
//                            for(int j = 0; j < TAILLE; j++)
//                            {
//
//                                if(TabMin[i][j] != nullptr)
//                                {
//                                    std::cout << TabMin[i][j]->getSymbole();
//                                }
//                                else std::cout << ".";
//                            }
//                            std::cout << std::endl;
//                        }
//                    }
//
//                }
//
//                std::cout << "m_min : " << m_min << std::endl;
//                std::cout << "m_maxAdv : " << m_maxAdv << std::endl;
//
//                if(m_min > m_maxAdv)
//                {
//                    m_maxAdv = m_min;
//                    CaseMaxAdv = CaseMin;
//                    TabMaxAdv = TabMin;
//
//                    std::cout << "WOW TabMaxAdv est : (1)" << std::endl;
//                    for(int i = 0; i < TAILLE; i++)
//                    {
//                        for(int j = 0; j < TAILLE; j++)
//                        {
//                            if(TabMaxAdv[i][j] != nullptr)
//                            {
//                                std::cout << TabMaxAdv[i][j]->getSymbole();
//                            }
//                            else std::cout << ".";
//                        }
//                        std::cout << std::endl;
//                    }
//                }
//
//            }
//
//            std::cout << "ATTENTION 7" << std::endl;
//            if(m_maxAdv > m_max)
//            {
//                std::cout << "ATTENTION 7.1" << std::endl;
//                m_max = m_maxAdv;
//                std::cout << "ATTENTION 7.2" << std::endl;
//                CaseMax = CaseMaxAdv;
//                std::cout << "ATTENTION 7.3" << std::endl;
//                TabMax = TabMaxAdv;
//
//                std::cout << "WOW tabMaxAdv est : (2)" << std::endl;
//                for(int i = 0; i < TAILLE; i++)
//                {
//                    for(int j = 0; j < TAILLE; j++)
//                    {
//                       if(TabMax[i][j] != nullptr)
//                        {
//                            std::cout << TabMax[i][j]->getSymbole();
//                        }
//                        else std::cout << ".";
//                    }
//                    std::cout << std::endl;
//                }
//
//
//                std::cout << "WOW tabMax est : (1)" << std::endl;
//                for(int i = 0; i < TAILLE; i++)
//                {
//                    for(int j = 0; j < TAILLE; j++)
//                    {
//                       if(TabMax[i][j] != nullptr)
//                        {
//                            std::cout << TabMax[i][j]->getSymbole();
//                        }
//                        else std::cout << ".";
//                    }
//                    std::cout << std::endl;
//                }
//
//            }
//
//            std::cout << "ATTENTION 7.4" << std::endl;
//
//
//            /*valAdv =  *max_element(minIA.begin(), minIA.end()); //source : http://stackoverflow.com/questions/9874802/how-can-i-get-the-max-or-min-value-in-a-vector
//            maxIA.push_back(valAdv);
//
//            for(int i = 0; i < minIA.size(); i ++)
//            {
//                if(minIA[i] > m_max)
//                {
//                    valAdv = min[i];
//                    CaseMax =
//                    TabMax =
//                }
//            }*/
//
//            std::cout << "ATTENTION 7.5" << std::endl;
//
//        }
//
//        //quelque part au milieu du code
//        // m_possibiliteTableau[elemJouable] = eval(elemJouable, m_possibiliteCompteur[CaseJouable]);
//
//        std::cout << "ATTENTION 7.9" << std::endl;
//        std::cout << "WOW tabMax est : (2)" << std::endl;
//        for(int i = 0; i < TAILLE; i++)
//        {
//            for(int j = 0; j < TAILLE; j++)
//            {
//               if(TabMax[i][j] != nullptr)
//                {
//                    std::cout << TabMax[i][j]->getSymbole();
//                }
//                else std::cout << " ";
//            }
//            std::cout << std::endl;
//        }
//
//        Recherche caseSelect(m_couleurIA, TabMax);
//
//        std::cout << "ATTENTION 8" << std::endl;
//
//        return minMax(caseSelect.getCoupsjouables(), TabMax, profondeur-1);
//
//    }
//
//        /*        if(valIA > m_max)
//            {
//                m_max = val;
//                m_CaseOuJouer = elemIA;
//            }
//
//            //niveau 0*/
//
//    //SANS OUBLIE LA RECURISIVITE
//
//    //on test pour tous les coups jouables de l'ia, quels peuvent etre les possibilit�s de l'adversaire
//
//
//    //pour chaque case possible on r�cup�re le tableau des coups possibles de l'adversaire
//
//
//    //et quels sont nos possibilit�sn de jeu ensuite � partir des possibilit�s de l'adversaire
//
//    //on s�lectionne le maximum des minimums de chaque branche (par le calcul de chaque coup possible avec eval() )
//
//    //on donne cet valeur pour chaque coupsjouables
//
//    //on s�lectionne le maximum de ces coupsjouables
//
////    std::cout<<"IA VA JOUER EN ("<<m_CaseOuJouer.getLig()+1<<","<<m_CaseOuJouer.getCol()+1<<")"<<std::endl<<"directions :";
////    for(auto elem:m_CaseOuJouer.getDirections())
////    {
////        std::cout<<elem<<";"<<std::endl;
////    }
////    system("pause");
//
//}
//*/



int IA::Min(Noeud* noeud, int profondeur)
{
    Noeud* tempNode=nullptr;

    //on prend la valeur la plus haute possible pour �tre s�r d'en trouver une inf�rieur
    int best = INT_MAX;
    //pour le moment c'est � 0
    int temp  = 0;
    if(profondeur == 0)
    {
        //on appel l'heuristique
        m_value = eval(noeud);
        noeud->setValeur(m_value);

        //std::cout << "profondeur 0" << std::endl;

        return m_value;
    }

    noeud->creerenfants();

    if(noeud->getEnfants().size() == 0)
    {
        m_value = eval(noeud);
        noeud->setValeur(m_value);
        return m_value;
    }

    for(const auto & elem : noeud->getEnfants())
    {
        temp = Max(elem, profondeur-1);
        elem->setValeur(temp);
        if (temp < best)
        {
            best = temp;
            m_value = best;
            m_sauv[best] = elem->getCoupDejaJoue() ;
            tempNode = elem;

            if(profondeur == 5)
            {
                CaseJouable Case(elem->getCoupDejaJoue());
                m_CaseOuJouer = Case;
                /*std::cout << "Min : profondeur " << profondeur << " best : " << best << "    " ;
                std::cout << "Lig : " << m_sauv[best]->getLig()+1 << " Col : " << m_sauv[best]->getCol()+1 << std::endl;
                std::cout << "Lig : " << m_CaseOuJouer.getLig()+1 << " Col : " << m_CaseOuJouer.getCol()+1 << std::endl;*/
            }
            //m_unorderedsauv[best] = noeud->getCoupDejaJoue();
            //m_unorderedsauv.insert(noeud->getCoupDejaJoue());
        }
    }

    tempNode->setEstMeilleur(true);

    return best;
}

int IA::Max(Noeud* noeud, int profondeur)
{
    Noeud* tempNode=nullptr;

    int best = INT_MIN;
    int temp  = 0;

    if(profondeur == 0)
    {
        //on appel l'heuristique
        m_value = eval(noeud);
        noeud->setValeur(m_value);

        //std::cout << "profondeur 0" << std::endl;
        return m_value;
    }

    noeud->creerenfants();

    if(noeud->getEnfants().size() == 0)
    {
        m_value = eval(noeud);
        noeud->setValeur(m_value);
        return m_value;
    }

    for(const auto & elem : noeud->getEnfants())
    {
        temp = Min(elem, profondeur-1);
        elem->setValeur(temp);

        if(temp > best)
        {
            best = temp;
            m_value = best;
            m_sauv[best] = elem->getCoupDejaJoue();
            tempNode = elem;


            if(profondeur == 5)
            {

                CaseJouable Case(elem->getCoupDejaJoue());
                m_CaseOuJouer = Case;
                /*std::cout << "Max : profondeur " << profondeur << " best : " << best << "    " ;
                std::cout << "Lig : " << m_sauv[best]->getLig()+1 << " Col : " << m_sauv[best]->getCol()+1 << std::endl;
                std::cout << "Lig : " << m_CaseOuJouer.getLig()+1 << " Col : " << m_CaseOuJouer.getCol()+1 << std::endl;*/
            }

            //m_unorderedsauv[best] = noeud->getCoupDejaJoue();
            //m_unorderedsauv.insert(noeud->getCoupDejaJoue());
        }
    }

    tempNode->setEstMeilleur(true);

    return best;
}


CaseJouable IA::minMax2(std::vector<std::vector<Pion*>> plateau,int profondeur,bool afficherArbre)
{
    Noeud racine(plateau, 0, m_couleurIA);

    initForce();

    Max(&racine, profondeur);

 //   std::cout << "fin minmax2 : " << valAJouer << std::endl;

    //CaseJouable select(m_sauv[valAJouer]); //m_sauv est une map de int et CaseJouable* donc ici on utilise le constructeur par copie afin de return un objet

    //CaseJouable select(m_unorderedsauv[valAJouer]);

  //  std::cout << "val : " << valAJouer << std::endl;

  /*  std::cout << "Lig : " << m_sauv[valAJouer]->getLig()+1 << " Col : " << m_sauv[valAJouer]->getCol()+1 << std::endl;

    std::cout << "Lig : " << select.getLig()+1 << " Col : " << select.getCol()+1 << std::endl;

    for(const auto & elem : m_sauv)
    {
        std::cout << elem.first << std::endl;
    }*/

  //  system("pause");
    if(afficherArbre)
    {
        Arbre a(&racine);
        a.afficherArbre(30,0);
        getch();
    }
    return m_CaseOuJouer;
}

CaseJouable IA::appelNegaMAx(std::vector<std::vector<Pion*>> plateau,int profondeur,bool afficherArbre)
{
    Noeud racine(plateau, 0, m_couleurIA);

    initForce();

    negaMax(&racine,profondeur);
    if(afficherArbre)
    {
        Arbre a(&racine);
        a.afficherArbre(30,0);
        getch();
    }
    return m_CaseOuJouer;
}

int IA::negaMax(Noeud* noeud, int profondeur)
{
    Noeud* tempNode=nullptr;

    int best = INT_MIN;
    int temp  = 0;

    if(profondeur == 0)
    {
        m_value = eval(noeud);
        noeud->setValeur(m_value);
        return m_value;
    }

    noeud->creerenfants();

    if(noeud->getEnfants().size() == 0)
    {
        m_value = eval(noeud);
        noeud->setValeur(m_value);
        return m_value;
    }

    for(const auto & elem : noeud->getEnfants())
    {
        temp = - negaMax(elem, profondeur-1);
        elem->setValeur(temp);
        if(temp > best)
        {
            best = temp;
            m_value = best;
            m_sauv[best] = elem->getCoupDejaJoue();
            tempNode = elem;


            if(profondeur == 5)
            {
                CaseJouable Case(elem->getCoupDejaJoue());
                m_CaseOuJouer = Case;
            }
        }
    }

    tempNode->setEstMeilleur(true);

    return best;
}


/* Fonction qui retourne le nombre de pi�ce retourner par la case jouable envoy� et set le plateau de jeu dans l'�tat si cette case est jou�e
*
*/

int IA::eval(Noeud* noeud)
{
    int compteur = 0;

/*    std::vector<int> directions = caseTest.getDirections();

    //je reprend la boucle d'ajouterPion car elle me permet de simuler le tableau et de compter le nombre de pi�ces qui changent
    plateauEtat[caseTest.getLig()][caseTest.getCol()] = new Pion(m_couleurIA);
    ///Boucle de changement de couleurs des pions pour chaque directions possible
    for(const auto& elem:directions)
    {
        //std::cout << "1" << std::endl;
        //init variables
        int i=caseTest.getLig();
        int j=caseTest.getCol();
        bool finboucle = false;
        //boucle de retournement pour une direction
        while(finboucle == false)
        {
            //std::cout << "2" << std::endl;
            //calcul des nouvelles coords en fct de la direction
            calcNewCoord(i,j,elem);
            if(calcIsInTab(i,j))
            {
                if(plateauEtat[i][j] != nullptr)
                {
                    //std::cout << "3" << std::endl;
                    //si la couleur du pion est differente de celle du joueur...
                    if(plateauEtat[i][j]->getCouleur() != m_couleurIA)
                    {
                        //std::cout << "4" << std::endl;
                        //... alors je change la couleur du pion
                        plateauEtat[i][j]->setCouleur(m_couleurIA);
                        if(m_couleurIA == COLOR_WHITE )plateauEtat[i][j]->setSymbole('B');
                        if(m_couleurIA == COLOR_BLACK )plateauEtat[i][j]->setSymbole('N');
                        compteur++;
                    }
                    //... sinon je suis tomb� sur le pion du joueur situ� � l'extr�mit� du parcours
                    // je met donc fin a la boucle de retournement des pions
                    else
                    {
                        finboucle = true;
                    }
                }
            }
        }
    }
    std::cout << "compteur : " << compteur << std::endl;
    return compteur;*/

    compteur = noeud->getNbPionRetourne();

/*    if( ( (noeud->getCoupDejaJoue()->getCol() || noeud->getCoupDejaJoue()->getLig()) < 0) || ((noeud->getCoupDejaJoue()->getCol() || noeud->getCoupDejaJoue()->getLig()) > 7) )
    {


    std::cout << "eval : col : " << noeud->getCoupDejaJoue()->getCol() << std::endl;
    std::cout << "eval : lig : " << noeud->getCoupDejaJoue()->getLig() << std::endl;

    }*/

    compteur *= 5; //coeff 5 pour le nombre de cases retourn�es et coeff 1 pour la position

    compteur += m_force[noeud->getCoupDejaJoue()->getCol()][noeud->getCoupDejaJoue()->getLig()];

    return compteur;
}


bool IA::calcIsInTab(int lig, int col)
{
    return (lig>=0 && lig<8 && col>=0 && col<8);
}

void IA::calcNewCoord(int &lig,int &col, int d)
{
    switch(d)
    {
    case 8 :
        lig-=1;
        break;
    case 9 :
        lig-=1;
        col+=1;
        break;
    case 6 :
        col+=1;
        break;
    case 3 :
        lig+=1;
        col+=1;
        break;
    case 2 :
        lig+=1;
        break;
    case 1 :
        lig+=1;
        col-=1;
        break;
    case 4 :
        col-=1;
        break;
    case 7 :
        lig-=1;
        col-=1;
        break;
    }
}

void IA::copieplateau(std::vector<std::vector<Pion*>>& copie, std::vector<std::vector<Pion*>>& origine)
{
    copie.resize(8, std::vector<Pion*>(1 << 7,0));

    for(int i = 0; i<TAILLE; i++)
    {
        for(int j = 0; j<TAILLE; j++)
        {
            if(origine[i][j] != nullptr)
            {
                copie[i][j] = new Pion(origine[i][j]->getCouleur());
            }
            else copie[i][j] = nullptr;

        }
    }

}

void IA::initForce()
{
    m_force[0][0] =500;
    m_force[0][1] =-150;
    m_force[0][2] =30;
    m_force[0][3] =10;
    m_force[0][4] =10;
    m_force[0][5] =30;
    m_force[0][6] =-150;
    m_force[0][7] =500;
    m_force[1][0] =-150;
    m_force[1][1] =-250;
    m_force[1][2] =0;
    m_force[1][3] =0;
    m_force[1][4] =0;
    m_force[1][5] =0;
    m_force[1][6] =-250;
    m_force[1][7] =-150;
    m_force[2][0] =30;
    m_force[2][1] =0;
    m_force[2][2] =1;
    m_force[2][3] =2;
    m_force[2][4] =2;
    m_force[2][5] =1;
    m_force[2][6] =0;
    m_force[2][7] =30;
    m_force[3][0] =10;
    m_force[3][1] =0;
    m_force[3][2] =2;
    m_force[3][3] =16;
    m_force[3][4] =16;
    m_force[3][5] =2;
    m_force[3][6] =0;
    m_force[3][7] =10;
    m_force[4][0] =10;
    m_force[4][1] =0;
    m_force[4][2] =2;
    m_force[4][3] =16;
    m_force[4][4] =16;
    m_force[4][5] =2;
    m_force[4][6] =0;
    m_force[4][7] =10;
    m_force[5][0] =30;
    m_force[5][1] =0;
    m_force[5][2] =1;
    m_force[5][3] =2;
    m_force[5][4] =2;
    m_force[5][5] =1;
    m_force[5][6] =0;
    m_force[5][7] =30;
    m_force[6][0] =-150;
    m_force[6][1] =-250;
    m_force[6][2] =0;
    m_force[6][3] =0;
    m_force[6][4] =0;
    m_force[6][5] =0;
    m_force[6][6] =-250;
    m_force[6][7] =-150;
    m_force[7][0] =500;
    m_force[7][1] =-150;
    m_force[7][2] =30;
    m_force[7][3] =10;
    m_force[7][4] =10;
    m_force[7][5] =30;
    m_force[7][6] =-150;
    m_force[7][7] =500;
}
